node-chart.js (3.9.1+~cs3.1.2-3) unstable; urgency=medium

  * Team upload
  * Fix clean
  * Fix install and then autopkgtest (Closes: #1039918, #1041217)

 -- Yadd <yadd@debian.org>  Sun, 13 Aug 2023 08:18:32 +0400

node-chart.js (3.9.1+~cs3.1.2-2) unstable; urgency=medium

  * Reupload to unstable
  * Bump Standards-Version to 4.6.2 (no changes needed)

 -- Pirate Praveen <praveen@debian.org>  Tue, 13 Jun 2023 01:38:30 +0530

node-chart.js (3.9.1+~cs3.1.2-1) experimental; urgency=medium

  * Add chartjs-plugin-annotation and use api.github.com in watch
  * Force 0.2 version of kurkle-color
  * New upstream version 3.9.1+~cs3.1.2
  * Build chartjs-plugin-annotation browser files using rollup
  * Install chartjs-plugin-annotation browser files with libjs-chart.js
  * Add copyright for chartjs-plugin-annotation

 -- Pirate Praveen <praveen@debian.org>  Sun, 26 Feb 2023 00:18:27 +0530

node-chart.js (3.9.1+~0.2.1-2) unstable; urgency=medium

  * Team upload
  * Add fix for rollup 3 (Closes: #1022585)

 -- Yadd <yadd@debian.org>  Tue, 25 Oct 2022 09:02:41 +0200

node-chart.js (3.9.1+~0.2.1-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.9.1+~0.2.1
  * Refresh patches

 -- Yadd <yadd@debian.org>  Wed, 10 Aug 2022 07:31:40 +0200

node-chart.js (3.8.2+~0.2.1-1) unstable; urgency=medium

  * Team upload
  * Use dh_nodejs_autodocs
  * Declare compliance with policy 4.6.1
  * New upstream version 3.8.2+~0.2.1
  * Update patch

 -- Yadd <yadd@debian.org>  Wed, 27 Jul 2022 22:18:17 +0200

node-chart.js (3.7.1+~0.1.9-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.7.1+~0.1.9

 -- Yadd <yadd@debian.org>  Wed, 23 Feb 2022 13:45:36 +0100

node-chart.js (3.7.0+~0.1.9-2) unstable; urgency=medium

  * Team upload
  * Add links to keep old names, thanks to Paul Gevers.
    (Closes: #1003016)

 -- Yadd <yadd@debian.org>  Mon, 03 Jan 2022 17:28:36 +0100

node-chart.js (3.7.0+~0.1.9-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.7.0+~0.1.9
  * Update lintian overrides
  * Back to unstable

 -- Yadd <yadd@debian.org>  Sun, 26 Dec 2021 11:25:38 +0100

node-chart.js (3.6.2+~0.1.9-1) experimental; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Remove unused license definitions for BSD-3-Clause.

  [ Yadd ]
  * Update standards version to 4.6.0, no changes needed.
  * Add ctype=nodejs to component(s)
  * Fix filenamemangle
  * Fix GitHub tags regex
  * Drop dependency to nodejs
  * Drop reproducible.patch, now included in upstream
  * Drop drop-legacy-plugins.patch, now included in upstream
  * Refresh patches
  * Add build denpendency to node-rollup-plugin-json
  * Update build
  * Replace chart-color* components by @kurkle/color
  * New upstream version 3.6.2+~0.1.9 (Closes: #1002112)
  * Build @kurkle/color
  * Install component in nodejs dir
  * Add lintian overrides
  * Update copyright

 -- Yadd <yadd@debian.org>  Wed, 22 Dec 2021 10:24:06 +0100

node-chart.js (2.9.4+dfsg+~cs2.10.1-3) unstable; urgency=medium

  * Add forwarded info to patch
  * Update minimum version of node-rollup-plugin-node-resolve to 11

 -- Pirate Praveen <praveen@debian.org>  Fri, 01 Jan 2021 14:03:59 +0530

node-chart.js (2.9.4+dfsg+~cs2.10.1-2) unstable; urgency=medium

  * Drop legacy node-resolve and commonjs plugins
  * Use debian/nodejs/extlinks to help rollup resolve some modules
  * Bump Standards-Version to 4.5.1 (no changes needed)
  * Drop debian/build_modules and add Depends node-rollup-plugin-terser

 -- Pirate Praveen <praveen@debian.org>  Wed, 30 Dec 2020 00:24:23 +0530

node-chart.js (2.9.4+dfsg+~cs2.10.1-1) unstable; urgency=medium

  * Team upload
  * Use uscan checksum
  * New upstream version 2.9.4+dfsg+~cs2.10.1
    Updates: chart.js only (Closes: CVE-2020-7746)

 -- Xavier Guimard <yadd@debian.org>  Fri, 06 Nov 2020 22:28:20 +0100

node-chart.js (2.9.3+dfsg-3) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Declare compliance with policy 4.5.0
  * Use dh-sequence-nodejs
  * Add node-supports-color in build dependencies (fixes debci)
  * Replace embedded build modules (@babel/code-frame) by a build dependency
    to node-babel7
  * Add "Multi-Arch: foreign"

 -- Xavier Guimard <yadd@debian.org>  Sun, 04 Oct 2020 15:26:42 +0200

node-chart.js (2.9.3+dfsg-2) unstable; urgency=medium

  * Team upload
  * Add reproducible patch. Thanks to Chris Lamb (Closes: #946333)

 -- Xavier Guimard <yadd@debian.org>  Sat, 07 Dec 2019 12:08:27 +0100

node-chart.js (2.9.3+dfsg-1) unstable; urgency=medium

  * Team upload
  * Bump debhelper compatibility level to 12
  * Declare compliance with policy 4.4.1
  * Add "Rules-Requires-Root: no"
  * Replace debian/node_modules/* by uscan components
  * New upstream version 2.9.3+dfsg
  * Switch install and minimal test to pkg-js-tools
  * Remove patch
  * Use upstream rollup.config.js:
    + Embed build modules: @babel/code-frame, jest-worker,
      rollup-plugin-terser and serialize-javascript
    + add patches to fix rollup paths and skip css minification
  * Add debian/minify to reproduce upstream css minification
  * Update debian/clean
  * Update copyrights
  * Dependencies:
    + require node-moment >= 2.24.0+ds-2~
    + add node-clean-css, node-merge-stream and node-terser in build
      dependencies
  * Ignore lintian false positives

 -- Xavier Guimard <yadd@debian.org>  Tue, 03 Dec 2019 22:33:50 +0100

node-chart.js (2.7.3+dfsg-5) unstable; urgency=medium

  * Team upload
  * Revert 2.7.3+dfsg-4 changes due to Buster freeze, only keep node-chart.js
    fix (Closes: #930439)

 -- Xavier Guimard <yadd@debian.org>  Thu, 13 Jun 2019 14:06:40 +0200

node-chart.js (2.7.3+dfsg-4) unstable; urgency=medium

  * Team upload
  * Fix install (Closes: #930439)
  * Switch minimal test to pkg-js-tools
  * Fix homepage
  * Add description in patch
  * Remove unneeded versioned dependency

 -- Xavier Guimard <yadd@debian.org>  Wed, 12 Jun 2019 21:28:52 +0200

node-chart.js (2.7.3+dfsg-3) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Thu, 21 Feb 2019 17:36:51 +0530

node-chart.js (2.7.3+dfsg-2) experimental; urgency=medium

  * Drop unnecessary copyright notices
  * Embed chartjs-color-string and chartjs-color modules
  * Update dependencies in debian/control to include them in the bundle
  * Install embedded modules
  * Use rollup-plugin-node-resolve with custom paths (bundles all dependencies)
  * Add copyright notice for embedded modules

 -- Pirate Praveen <praveen@debian.org>  Sun, 17 Feb 2019 16:08:16 +0530

node-chart.js (2.7.3+dfsg-1) experimental; urgency=medium

  * New upstream version 2.7.3 (Closes: #896468)
  * Bump Standards-Version to 4.3.0 (no changes needed)
  * Run uglifyjs during build
  * Drop build deps used for previous gulp based build
  * Drop all embedded modules

 -- Pirate Praveen <praveen@debian.org>  Fri, 15 Feb 2019 12:05:30 +0530

node-chart.js (1.0.2-1) unstable; urgency=low

  * Initial release (Closes: #908325)

 -- Pirate Praveen <praveen@debian.org>  Sat, 08 Sep 2018 19:16:46 +0530
